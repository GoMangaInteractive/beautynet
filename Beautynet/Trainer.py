'''
    Beautynet AI - A neural network that can recognize beauty

    Copyright (C) 2018 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from __future__ import division, print_function, absolute_import

import tensorflow
from skimage import color, io
from scipy.misc import imresize
import numpy as np
from sklearn.model_selection import train_test_split
import os
from glob import glob

import tflearn
from tflearn.data_utils import shuffle, to_categorical
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
from tflearn.metrics import Accuracy

class TrainerData(object):
    """
        TrainerData Class:
            Is used to store the images and the labels for the training sample
    """

    def __init__(self, positiveFiles, negativeFiles, filesCount, fileSize = 64):
        """
            TrainerData class constructor:
                Initializes the class loading all the images assigning a label
        """

        self.Images = np.zeros((filesCount, fileSize, fileSize, 3), dtype='float64')
        self.Labels = np.zeros(filesCount)

        count = 0
        
        #Load all the meme files with the label 1
        for f in positiveFiles:
            try:
                img = io.imread(f)
                new_img = imresize(img, (fileSize, fileSize, 3))
                self.Images[count] = np.array(new_img)
                self.Labels[count] = 1
                count += 1
            except:
                continue
        
        #Load all the non-meme files with the label 0
        for f in negativeFiles:
            try:
                img = io.imread(f)
                new_img = imresize(img, (fileSize, fileSize, 3))
                self.Images[count] = np.array(new_img)
                self.Labels[count] = 0
                count += 1
            except:
                continue

class Trainer (object):
    """
        Trainer Class:
            This class trains TensorFlow using train sample data and TFLearn
    """

    def __init__(self, baseDirectoryPositives = 'training/beautiful/', baseDirectoryNegatives = 'training/ugly/'):
        """
            Trainer class constructor:
                Initializes the trainer and loads all the images
        """
        #Loading file names
        beautiful = sorted(glob( os.path.join(baseDirectoryPositives, '*.jpg')))
        ugly = sorted(glob(os.path.join(baseDirectoryNegatives, '*.jpg')))

        filesCount = len(beautiful) + len(ugly)

        print("Sample data files count: " + str(filesCount))

        self.Samples = TrainerData(beautiful, ugly, filesCount)


    def StartTraining(self, epochs = 100, Verbose = True):
        """
            StartTraining function:
                This function setups the Neural Network and starts training with the sample data
        """

        #Split the tests
        images, images_test, labels, labels_test = train_test_split(self.Samples.Images, self.Samples.Labels, test_size=0.1, random_state=42)

        #Encode the labels
        labels = to_categorical(labels, 2)
        labels_test = to_categorical(labels_test, 2)

        #Normalize the images
        img_prep = ImagePreprocessing()
        img_prep.add_featurewise_zero_center()
        img_prep.add_featurewise_stdnorm()

        #Create extra synthetic training data by flipping & rotating images
        img_aug = ImageAugmentation()
        img_aug.add_random_flip_leftright()
        img_aug.add_random_rotation(max_angle=25.)

        #Input is a 64x64 image with 3 color channels (red, green and blue)
        network = input_data(shape=[None, 64, 64, 3], data_preprocessing=img_prep, data_augmentation=img_aug)

        #1: Convolution layer with 32 filters, each 3x3x3
        conv_1 = conv_2d(network, 32, 3, activation='relu', name='conv_1')

        #2: Max pooling layer
        network = max_pool_2d(conv_1, 2)

        #3: Convolution layer with 64 filters
        conv_2 = conv_2d(network, 64, 3, activation='relu', name='conv_2')

        #4: Convolution layer with 64 filters
        conv_3 = conv_2d(conv_2, 64, 3, activation='relu', name='conv_3')

        #5: Max pooling layer
        network = max_pool_2d(conv_3, 2)

        #6: Fully-connected 512 node layer
        network = fully_connected(network, 512, activation='relu')

        #7: Dropout layer to combat overfitting
        network = dropout(network, 0.5)

        #8: Fully-connected layer with two outputs
        network = fully_connected(network, 2, activation='softmax')

        #Configure how the network will be trained
        acc = Accuracy(name="Accuracy")
        network = regression(network, optimizer='adam', loss='categorical_crossentropy', learning_rate=0.0005, metric=acc)

        #Wrap the network in a model object
        model = tflearn.DNN(network, checkpoint_path='model_beauty.tflearn', max_checkpoints = 3, tensorboard_verbose = 0, tensorboard_dir='tmp/tflearn_logs/')

        print("\n\nStart Computing...")

        model.fit(images, labels, validation_set=(images_test, labels_test), batch_size=500, n_epoch=epochs, run_id='model_beauty', show_metric=Verbose)

        model.save('beautynet_model.tflearn')
